# Plate_Motobike_Haar_SVM
----
# Introduction
----
This project uses Haar cascade to detect plate's region and SVM to recognize character.
# Requirement
----
Download [images](https://drive.google.com/drive/folders/1YO8OMZx6v7WxMOe_EopNtWHJZ4aMbZFv?usp=sharing) and [training data](https://drive.google.com/drive/folders/1kpDcGUDODQp90UymcixM4fHFbJbB6uJl?usp=sharing)
Using the following script:
```
sudo chmod +x requirements.txt
./requirements.txt
```
# Result
----
#### FPS of algorithm:
![Result 1](result/2.jpg)
![Result 2](result/5.jpg)
# Usage
----
## 1. Training
Using the following syntax:
```
python3 train.py
```
Training set in data folder. The weight after training has stored in weight folder.
## 2. Testing:
Using the following command:
```
python3 plate.py [PATH_OF_IMAGE_FOLDER]
```
# References
----
## Repositories

