import cv2
import sys
import copy
import numpy as np 
import os
import random
from sklearn import svm
from sklearn.externals import joblib
from sklearn import metrics
import time

# Thư mục chứa file trọng số cho nhận diện ký tự
PATH_WEIGHT = "weight"
DEBUG = False

# Vị trí của mảng tương ứng với nhãn của ký tự, ví dụ vị trí 10 là nhãn A
LABELS = ['0','1','2','3','4','5','6','7','8','9', 'A','B','C','D','E',
        'F','G','H','I','J','K','L','M','N','O','P','Q',
        'R','S','T','U','V','W','X','Y','Z']

# Đọc ảnh, đưa đường dẫn của ảnh vào
im = cv2.imread(sys.argv[1])

start = time.time()

# Chuyển đổi ảnh BGR sang ảnh xám
im_gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

if DEBUG:
        cv2.imwrite("result/Gray.jpg", im_gray)

'''Bộ lọc phi tuyến, bảo toàn cạnh và loại nhiễu
Xem chi tiết tại: http://opencvexamples.blogspot.com/2013/10/applying-bilateral-filter.html'''
noise_removal = cv2.bilateralFilter(im_gray,9,75,75)

if DEBUG:
        cv2.imwrite("result/noise_removal.jpg", noise_removal)

'''Điều chỉnh độ tương phản dựa vào histogram ảnh
Xem chi tiết tại: https://www.geeksforgeeks.org/histograms-equalization-opencv/'''
equal_histogram = cv2.equalizeHist(noise_removal)

if DEBUG:
        cv2.imwrite("result/equal_histogram.jpg", equal_histogram)

'''Thuật toán opening, giảm egde nhiễu , egde thật thêm sắc nhọn bằng cv2.morphologyEx sử dụng kerel 5x5
Xem ví dụ hình ảnh tại: https://docs.opencv.org/trunk/d9/d61/tutorial_py_morphological_ops.html'''
kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
morph_image = cv2.morphologyEx(equal_histogram,cv2.MORPH_OPEN,kernel,iterations=20)

if DEBUG:
        cv2.imwrite("result/morph_image.jpg", morph_image)
# Xóa nền
sub_morp_image = cv2.subtract(equal_histogram, morph_image)

if DEBUG:
        cv2.imwrite("result/sub_morp_image.jpg", sub_morp_image)
# cv2.imshow("morph_image", morph_image)
# cv2.imshow("equal_histogram", equal_histogram)
# cv2.imshow("sub_morp_image", sub_morp_image)

# Dùng ngưỡng OTSU đưa ảnh về trắng đen tách biệt background và region interesting
ret,thresh_image = cv2.threshold(sub_morp_image,0,255,cv2.THRESH_OTSU)

if DEBUG:
        cv2.imwrite("result/thresh_image.jpg", thresh_image)

# Nhận biết cạnh
canny_image = cv2.Canny(thresh_image,250,255)

if DEBUG:
        cv2.imwrite("result/canny_image.jpg", canny_image)

# Tạo mảng kích thước 3x3 với tất cả các giá trị đều bằng 1
kernel = np.ones((3,3), np.uint8)

'''Thuật toán dilate
Xem ví dụ tại: https://docs.opencv.org/trunk/d9/d61/tutorial_py_morphological_ops.html'''
dilated_image = cv2.dilate(canny_image,kernel,iterations=1)

if DEBUG:
        cv2.imwrite("result/dilated_image.jpg", dilated_image)

# Tìm tất cả các contours trên ảnh
contours, hierarchy = cv2.findContours(dilated_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Sắp xếp các contours theo thứ tự giảm dần của diện tích
contours = sorted(contours, key = cv2.contourArea, reverse = True)[:10]

if DEBUG:
        # Tạo ma trận rộng có kích thước bằng kích thước ảnh đầu vào
        mask = np.zeros(im.shape, np.uint8)

        # Vẽ contour biển số đã lọc lên ảnh và hiển thị
        cv2.drawContours(mask, contours, -1, 255, -1)
        cv2.imwrite("result/contours.jpg", mask)

''' Tiếp theo ta tính chu vi của từng contour bẳng cv2.arcLength
sau đó dùng cv2.approxPolyDP để xấp xỉ đa giác ở đây ta cần tìm là hình chữ nhật
nên ta chỉ giữ lại contour nào có 4 cạnh '''
screenCnt = None
for c in contours:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.06 * peri, True) 
        if len(approx) == 4:
                screenCnt = approx
                break

if DEBUG:
        # Tạo ma trận rộng có kích thước bằng kích thước ảnh đầu vào
        mask = np.zeros(im.shape, np.uint8)

        # Vẽ contour biển số đã lọc lên ảnh và hiển thị
        cv2.drawContours(mask, [screenCnt], -1, 255, -1)
        cv2.imwrite("result/plate_contour.jpg", mask)

# Chuyển đổi tọa độ contour được xác định là biển số sang định dạng x,y,w,h
x,y,w,h = cv2.boundingRect(screenCnt)

# Cắt vùng biển số đã xác định trên ảnh và lưu vào roi
roi = im[y:y+h, x:x+w]

if DEBUG:
        cv2.imwrite("result/plate.jpg", roi)

# Chuyển ảnh BGR sang ảnh xám 
roi_gray = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)

if DEBUG:
        cv2.imwrite("result/roi_gray.jpg", roi_gray)

# Lọc nhiễu bằng GaussianBlur
roi_blur = cv2.GaussianBlur(roi_gray,(3,3),1)

if DEBUG:
        cv2.imwrite("result/roi_blur.jpg", roi_blur)

# Dùng THRESH_BINARY_INV đưa ảnh về trắng đen
ret,thre = cv2.threshold(roi_blur,125,255,cv2.THRESH_BINARY_INV)

if DEBUG:
        cv2.imwrite("result/roi_thre.jpg", thre)

'''Thuật toán dilate
Xem ví dụ tại: https://docs.opencv.org/trunk/d9/d61/tutorial_py_morphological_ops.html'''
kerel3 = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
thre_mor = cv2.morphologyEx(thre,cv2.MORPH_DILATE,kerel3)
thre_mor = cv2.dilate(thre_mor,kernel,iterations=1)

# Tìm tất cả các contours trên ảnh
cont, hier = cv2.findContours(thre_mor,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

# Xác định các contours là ký tự, biển xe ô tô VN có 8 ký tự
areas_ind = {}
areas = []
for ind,cnt in enumerate(cont) :
        area = cv2.contourArea(cnt)
        # print(area, roi_gray.shape[0] * roi_gray.shape[1])
        if((roi_gray.shape[0] * roi_gray.shape[1]) - area < 2000):
                continue
        else:
                areas_ind[area] = ind
                areas.append(area)
areas = sorted(areas,reverse=True)[:8]

# Tạo ma trận rộng có kích thước bằng kích thước ảnh đầu vào
mask_num = np.zeros(im.shape, np.uint8)

# Nhận diện ký tự vừa xác định
plate = []
for c,i in enumerate(areas):
        x1,y1,w1,h1 = cv2.boundingRect(cont[areas_ind[i]])
        if DEBUG:
                # Vẽ contour biển số đã lọc lên ảnh và hiển thị
                cv2.drawContours(mask_num, [cont[areas_ind[i]]], -1, 255, -1)
                
        char = roi[y1:y1+h1, x1:x1+w1]
        gray = cv2.cvtColor(cv2.resize(char,(30,30)), cv2.COLOR_BGR2GRAY)
        _, gray = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)

        # Đưa ảnh về kích thước 1x900
        gray = gray.reshape(1, gray.shape[0]*gray.shape[1])

        # cv2.imwrite(str(c) +".jpg", char)

        # Load trọng số của mạng đã training
        clf = joblib.load(PATH_WEIGHT + '/model.joblib')
        # print(clf.predict(feature)[0])

        # Dự đoán ký tự và đưa vào mảng plate ký tự đã dự đoán
        plate.append(clf.predict(gray)[0])

        # Vẽ khung chữ nhật bao quanh ký tự trên ảnh
        cv2.rectangle(im,(x+x1,y+y1),(x+x1+w1,y+y1+h1),(0,255,0),2)

        # Hiển thị ký tự trên ảnh
        startX = x+x1
        startY = y+y1 - 15 if y+y1 - 15 > 15 else y+y1 + 15
        cv2.putText(im, str(LABELS[plate[-1]]), (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)

if DEBUG:
        cv2.imwrite("result/num_contour.jpg", mask_num)

print("Time execute: ", time.time() - start)
cv2.imshow("plate", im)

if DEBUG:
        cv2.imwrite("result/result.jpg", im)
cv2.waitKey(0)