# Plate_Car_CV_SVM
----
# Introduction
----
This project uses opencv to detect plate's region and SVM to recognize character.
# Requirement
----
Download [training data](https://drive.google.com/drive/folders/1_Z_ueQqNliPNXvYDH2qMeaL8Nhn0jKa7?usp=sharing)
Using the following script:
```
sudo chmod +x requirements.txt
./requirements.txt
```
# Result
----
#### FPS of algorithm:
![Result 1](result/plate0/result.jpg)
![Result 2](result/plate1/result.jpg)
# Usage
----
## 1. Training
Using the following syntax:
```
python3 train.py
```
Training set in data folder. The weight after training has stored in weight folder.
## 2. Testing:
Using the following command:
```
python3 plate_test.py [PATH_OF_IMAGE]
```
# References
----
## Repositories

