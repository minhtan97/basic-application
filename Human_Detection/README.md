# Human detection
----
# Introduction
----
This project uses tensorflow API to detect human.
# Requirement
----
Tensorflow API.
Download weight (either): [Faster RCNN inception v2](https://drive.google.com/file/d/18KeDooSNoIhJWCqpxochr5HeAp5SEQOr/view?usp=sharing) or [SSD-MobileNet](https://drive.google.com/file/d/14KRke0YhhcGZ-02dcs9L4BM0CiU5P5y_/view?usp=sharing)
Using the following script:
```
sudo chmod +x requirements.txt
./requirements.txt
```
# Result
----
#### FPS of algorithm:
![Result 1](result/plate0/result.jpg)
![Result 2](result/plate1/result.jpg)
# Usage
----
## 1. Testing:
Using the following command:
```
python3 human.py [PATH_OF_VIDEO]
```
# References
----
## Repositories

